package ru.cloud.base.template.user.service.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
class UserMessageRestController {
	
	@Value("${message:Hello default}")
	private String message;
	
	@RequestMapping("/")
	public String getMessage() {
		return this.message;
	}
}